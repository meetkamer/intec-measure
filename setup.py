# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

'''
from wheel.bdist_wheel import bdist_wheel as _bdist_wheel
class bdist_wheel(_bdist_wheel):
    def finalize_options(self):
        _bdist_wheel.finalize_options(self)
        self.root_is_pure = False    
'''
         
readme = open('README.rst', 'r')
README_TEXT = readme.read()
readme.close()

    
setup(
    name='intec-measure',
    version='0.0.1',
    packages=['intec.measure', 'intec.measure.instruments'],
    python_requires='>=3.4',
    description='Intec Measure Library',
    long_description = README_TEXT,    
    url='https://bitbucket.org/zmic/intec-pymeasure',
    author='Michael Vanslembrouck',
    license='MIT', 
    #cmdclass={'bdist_wheel': bdist_wheel},    
    classifiers=[
        'Development Status :: 3 - Alpha',
    ],        
    #install_requires = ['numpy >= 1.11.1', 'matplotlib >= 1.5.1', 'intec-units >= 0.0.1'],    
    install_requires = ['intec-units >= 0.0.1'],    
    
)
      
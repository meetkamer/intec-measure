=================
What is it?
=================

Intec Photonics measurement framework



=================
How do I set it up?
=================

* pip install intec-pymeasure OR clone repository to your local hard disk and run setup.py

* required Python modules: see intec\measure\rollout\install-python-libs.ps1

=================
Example
=================

::

    from intec.units import MILLIWATT, NANOMETER

    def example1():

    def example2():



    if __name__ == '__main__':
        example1()
        example2()
            
    
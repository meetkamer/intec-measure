$pip = "c:\python37\scripts\pip.exe"

& $pip install --upgrade pip
& $pip install --upgrade numpy
& $pip install --upgrade scipy
& $pip install --upgrade matplotlib
& $pip install --upgrade pyvisa
& $pip install --upgrade pyserial
& $pip install --upgrade comtypes
& $pip install --upgrade pywin32
& $pip install --upgrade virtualenv
& $pip install --upgrade pylint
& $pip install --upgrade pandas
& $pip install --upgrade pyside2       

class InstrumentMetaclass(type):
    def __new__(cls, name, bases, D, **kwargs):
        T = super().__new__(cls, name, bases, D)
        return T
    @property
    def name(cls):
        return cls.__name__

class Instrument(metaclass = InstrumentMetaclass):
    def __init__(self, *args, **kwargs):
        pass

class Chassis(Instrument):
    pass

class Module(Instrument):
    def __init__(self, *args, **kwargs):
        pass

class Property:
    def __init__(self, *args, **kwargs):
        pass

def FixedProperty(*args, **kwargs):
    kwargs['fixed'] = True
    return Property(*args, **kwargs)

class CommandSet:
    def __init__(self, *args, **kwargs):
        pass
    def command(self, *args, **kwargs):
        pass

class Command:
    def __init__(self, *args, **kwargs):
        pass



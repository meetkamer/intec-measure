from .generator_base import Instrument, Chassis, Module

class InterfaceOutput:
    def write(self, L):
        print(L)

class InterfaceMetaclass(type):
    o = InterfaceOutput()
    def __new__(cls, name, bases, attrs, **kwargs): 
        T = super().__new__(cls, name, bases, attrs)
        if bases != ():
            if len(bases) != 1:
                raise RuntimeError("Interface must derive from a single parent class")
            T.i = kwargs['i']
            T.base = bases[0]
            if T.base is Interface:
                if not T.i in (Chassis, Module, Instrument):
                    if issubclass(T.i, Chassis):
                        T.base = InterfaceChassis
                    elif issubclass(T.i, Module):
                        T.base = InterfaceModule
                    elif issubclass(T.i, Instrument):
                        T.base = InterfaceInstrument
            if T.base != Interface:
                T.generate()
        return T
    @property
    def name(cls):
        return cls.i.name
    @property
    def basename(cls):
        return cls.base.name
    @classmethod
    def print(cls, *args, **kwargs):
        kwargs['file'] = cls.o
        print(*args, **kwargs)

def qprint(*args, **kwargs):
    InterfaceMetaclass.print(*args, **kwargs)

class Interface(metaclass=InterfaceMetaclass):
    @classmethod
    def generate(cls):
        print("Begin generation of", cls.name)
        qprint(f"class {cls.name}({cls.basename}):")
        
    
class InterfaceInstrument(Interface, i=Instrument):
    pass
class InterfaceModule(Interface, i=Module):
    pass
class InterfaceChassis(Interface, i=Chassis):
    pass




    

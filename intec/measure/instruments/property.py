import re
from .instrument_base import InstrumentRegisteredAttribute

def regexp_match(regexp, str):
    mo = regexp.search(str)
    return mo.group(1)

class InstrumentProperty(InstrumentRegisteredAttribute):
    
    class EXEC1(tuple):
        def __call__(self, obj, value):
            return self[0](self[1], obj, value)

    class OR(tuple):
        def __call__(self, obj, value):
            errors = []
            for x in self:
                r, v = x(obj, value)
                if r is None:
                    return None, v
                errors.append(r)
            return ValueError(' | '.join([ str(e) for e in errors ])), None

    class AND(tuple):
        def __call__(self, obj, value):
            for x in self:
                r, value = x(obj, value)
                if not r is None:
                    return r, None
            return None, value

    ################################################################################################    

    @staticmethod
    def min_checker__(min_value, obj, value):
        try:
            if value < min_value:
                error_string = "Specified quantity (%s) is below minimum supported by the device (%s)." % (value, min_value)
                return ValueError( error_string ), None
        except Exception as e:
            return e, None
        return None, value
            
    @staticmethod
    def max_checker__(max_value, obj, value):
        try:
            if value > max_value:
                error_string = "Specified quantity (%s) is above maximum supported by the device (%s)." % (value, max_value)
                return ValueError( error_string ), None
        except Exception as e:
            return e, None
        return None, value
                           
    @staticmethod
    def range_checker__(range, obj, value):
        try:
            if value < range[0] or value > range[1]:
                error_string = "Specified quantity (%s) is outside range supported by the device [%s,%s]." % (value, range[0], range[1])
                return ValueError( error_string ), None
        except Exception as e:
            return e, None
        return None, value

    @staticmethod
    def unit_get__(unit, obj, value):
        try:
            return None, value*unit
        except Exception as e:
            return e, None
                           
    @staticmethod
    def unit_set__(wanted_unit, obj, value):
        try:
            in_unit = value.unit
        except AttributeError:
            error_string = "value should have unit %s or be convertible to that unit"%wanted_unit
            return ValueError(error_string), value            
        if in_unit != wanted_unit:
            try:
                value = value.to(wanted_unit)
            except Exception as e:
                return e, None
        return None, value.m

    @staticmethod
    def allowed_values_set__(allowed, obj, value):
        try:
            result = value in allowed
        except ValueError:
            result = False
        if not result:
            if type(value) == str:
                error_string = "Specified value (%r) should be one of %r" % (value, allowed)
            else:
                error_string = "Specified value (%s) should be one of [%s]" % (value, ', '.join( [str(x) for x in allowed]))                    
            return ValueError( error_string ), None
        if type(allowed) is dict:
            value = allowed[value]
        return None, value    

    @staticmethod
    def allowed_values_get__(allowed, obj, value):
        return None, value
        
    ################################################################################################    
    def create_prefab_getter(self):
        prefab = self.kwargs.get('prefab')
        if prefab:
            if not isinstance(prefab, tuple):
                self.getter = prefab.getter()
            elif len(prefab) == 3:
                type, fs, regexp = prefab
                regexp = re.compile(regexp)
                fs += '?'                                
                if type == float:
                    def __getter__(o):
                        data = o.query(fs)
                        return float(regexp_match(regexp, data))
                    self.getter = __getter__
                elif type == int:
                    def __getter__(o):
                        data = o.query(fs)
                        return int(regexp_match(regexp, data))
                    self.getter = __getter__
                elif type == str:
                    def __getter__(o):
                        data = o.query(fs).rstrip()
                        return regexp_match(regexp, data)
                    self.getter = __getter__
                else:
                    raise TypeError("Unknown prefab type")                      
            else:
                type, fs = prefab
                fs += '?' 
                if type == float:
                    def __getter__(o):
                        return float(o.query(fs))
                    self.getter = __getter__
                elif type == str:
                    def __getter__(o):
                        return o.query(fs).rstrip()
                    self.getter = __getter__
                elif type == bool:
                    def __getter__(o):
                        return int(o.query(fs))!=0
                    self.getter = __getter__
                elif type == int:
                    def __getter__(o):
                        return int(o.query(fs))
                    self.getter = __getter__
                else:
                    raise TypeError("Unknown prefab type")    
    
    def create_prefab_setter(self):
        prefab = self.kwargs.get('prefab')
        if prefab:
            if not isinstance(prefab, tuple):
                self.setter = prefab.setter()        
            else:
                type, fs, *dummy = prefab            
                if type == float:
                    fs += ' %f' 
                    def __setter__(o, v):
                        return o.write(fs%v)
                    self.setter = __setter__
                elif type == str:
                    fs += ' %s' 
                    def __setter__(o, v):
                        return o.write(fs%v)
                    self.setter = __setter__            
                elif type == bool or type == int:
                    fs += ' %i' 
                    def __setter__(o, v):
                        return o.write(fs%v)
                    self.setter = __setter__            
                else:
                    raise TypeError("Unknown prefab type")

    ################################################################################################    
    def is_read_only(self):
        return self.kwargs.get("read_only", False)

    def is_write_only(self):
        return self.kwargs.get("write_only", False)

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        set_nodes = []
        get_nodes = []
        self.append__( set_nodes, "min_value", InstrumentProperty.min_checker__ )
        self.append__( set_nodes, "max_value", InstrumentProperty.max_checker__ )
        self.append__( set_nodes, "range", InstrumentProperty.range_checker__ )
        self.append__( set_nodes, "unit", InstrumentProperty.unit_set__ )
        self.append__( get_nodes, "unit", InstrumentProperty.unit_get__ )
        if "choice" in kwargs:
            node = InstrumentProperty.EXEC1( (InstrumentProperty.allowed_values_set__, kwargs["choice"]) )
            if set_nodes:
                if len(set_nodes) > 1:
                    set_nodes = [InstrumentProperty.OR(( InstrumentProperty.AND(set_nodes), node))]                
                else:
                    set_nodes = [InstrumentProperty.OR((set_nodes[0], node))]                
            else:
                set_nodes = [node]
            if get_nodes:
                node = InstrumentProperty.EXEC1( (InstrumentProperty.allowed_values_get__, kwargs["choice"]) )
                if len(get_nodes) > 1:
                    get_nodes = [InstrumentProperty.OR(( InstrumentProperty.AND(get_nodes), node))]                
                else:
                    get_nodes = [InstrumentProperty.OR((get_nodes[0], node))]                
            #else:
            #    get_nodes = [node]

        if set_nodes: 
            if len(set_nodes) > 1:
                self.before_set__ = InstrumentProperty.AND(set_nodes)
            else:
                self.before_set__ = set_nodes[0]
        if get_nodes: 
            if len(get_nodes) > 1:
                self.after_get__ = InstrumentProperty.AND(get_nodes)
            else:
                self.after_get__ = get_nodes[0]

    def append__(self, nodelist, keyword, static_function ):
        if keyword in self.kwargs:
            nodelist.append( InstrumentProperty.EXEC1( (static_function, self.kwargs[keyword]) ) )

    def bind_to_class__(self, cls, name):
        self.name = name
        if not self.is_write_only():
            try:
                self.getter = getattr( cls, self.getter_name() )
            except AttributeError:
                self.create_prefab_getter()        
        if not self.is_read_only():
            try:
                self.setter = getattr( cls, self.setter_name() )
            except AttributeError:
                self.create_prefab_setter()        

    ################################################################################################    
    def getter_name(self):
        return "%s_get__"%self.name
        
    def setter_name(self):
        return "%s_set__"%self.name
        
    ################################################################################################    
    def after_get__(self, obj, value):
        return None, value

    def __get__(self, obj, type = None):
        if obj is None: 
            return self
        try:
            getter = self.getter            
        except AttributeError:
            if self.is_write_only():
                error_string = "Property %r is write-only"%(self.name)  
                raise TypeError(error_string)
            else:
                error_string = "%s must implement %s"%(obj.__class__, self.getter_name())
                raise NotImplementedError(error_string)  
        value = getter(obj)             
        e, value = self.after_get__(obj, value)
        if e:
            raise e
        return value

    ################################################################################################   
    def before_set__(self, obj, value):
        return None, value

    def __set__(self, obj, value):
        e, value = self.before_set__(obj, value)
        if e:
            raise e
        try:
            setter = self.setter
        except AttributeError:
            if self.is_read_only():
                error_string = "Property %r is read-only"%(self.name)  
                raise TypeError(error_string)
            else:
                error_string = "%s must implement %s"%(obj.__class__, self.setter_name())  
                raise NotImplementedError(error_string)  
        setter(obj, value)

    ################################################################################################   
    def __delete__(self, instance):
        raise RuntimeError


InstrumentSetting = InstrumentProperty
def InstrumentEvent(*args, **kwargs):
    kwargs['read_only'] = True
    return InstrumentProperty(*args, **kwargs)

InstrumentQuery = InstrumentEvent



import time
from intec.units.si import AMPERE, MILLIAMPERE, MICROAMPERE, VOLT, MILLIVOLT
from intec.measure.instruments.Keithley.voltage_current_source import Keithley2400


def example1():
    K = Keithley2400(address = "GPIB::2")
    print(K.ask("*IDN?"))
    print(K.is_on())
    return

    '''
    print K.measure_current().get_value(MICROAMPERE)
    #print K.measure_voltage().get_value(MILLIVOLT)
    print K.current
    print K.voltage
    K.source_voltage_limit = 20 * VOLT
    print K.source_voltage_limit
    K.mode = 'voltage'
    print K.mode   
    K.current_limit = 0.01*AMPERE
    print K.current_limit
    K.voltage = 0.1*VOLT
    print K.measure_current().get_value(MILLIAMPERE)
    K.voltage = 0.2*VOLT
    print K.measure_current().get_value(MILLIAMPERE)
    K.voltage = 1*VOLT
    print K.measure_current().get_value(MILLIAMPERE)
    print K.voltage
    '''
    
    

example1()


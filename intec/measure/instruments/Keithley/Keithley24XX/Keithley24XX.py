from intec.measure.instruments.instrument import VisaInstrument
from intec.measure.instruments.property import InstrumentProperty
from intec.units.si import AMPERE, VOLT


class Keithley24XX(VisaInstrument):
    def __init_instrument__(self):
        answer = self.ask(":SYSTem:RSENse?")
        if answer == '1':
            self.write(":SYSTem:RSENse 0")

    mode = InstrumentProperty(allowed_values = ['voltage', 'current', 'Voltage', 'Current'])
    def __mode_set__(self, mode):
        mode = mode.lower()
        if mode == 'voltage':
                self.write(":SOUR:FUNC VOLT")
                self.write(":SOUR:VOLT:MODE FIX")
                self.write(':SENS:FUNC "CURR"')
                self.__mode__ = 'Voltage'                
        elif mode == 'current':
                self.write(":SOUR:FUNC CURR")
                self.write(":SOUR:CURR:MODE FIX")
                self.write(':SENS:FUNC "VOLT"')
                self.__mode__ = 'Current'
    def __mode_get__(self):
        return self.__mode__
        
    def switch_off(self):    
        return self.switch_on(False)
    def switch_on(self, value = True):       
        if value:
            self.write(":OUTP ON")
        else:
            self.write(":OUTP OFF")
    def is_on(self):
        return int(self.ask(":OUTP?")) != 0
                
    def measure_current(self):
        answer = self.ask("MEAS:CURR?")
        answer = answer.split(',')
        return float(answer[1]) * AMPERE
    def measure_voltage(self):
        answer = self.ask("MEAS:VOLT?")
        answer = answer.split(',')[0]
        return float(answer) * VOLT
        
        
                
class Keithley2400(Keithley24XX):
    '''This command is used to set the Over Voltage Protection (OVP) limit
for the V-Source. The V-Source output will not exceed the selected
limit. An exception to this is a parameter value that exceeds 160V for
the Model 2400, 500V for the Model 2410, 48V for the Model 2420,
80V for the Models 2425 and 2430, or 32V for the Model 2440.
Exceeding those values allows the V-Source to output its maximum
voltage. The OVP limit is also enforced when in the I-Source Mode.
The limit parameter values are magnitudes and are in effect for both
positive and negative output'''
    source_voltage_range = InstrumentProperty(unit=VOLT, range=(-210*VOLT, 210*VOLT), prefab=(float, ":SOURCE:VOLT:RANGE") )        
    source_current_range = InstrumentProperty(unit=AMPERE, range=(-1.05*AMPERE, 1.05*AMPERE), prefab=(float, ":SOURCE:CURR:RANGE") )    
    
    '''compliance limits'''
    voltage_limit = InstrumentProperty(unit=VOLT, range=(-210*VOLT, 210*VOLT), prefab=(float, ":SENS:VOLT:PROT") )    
    current_limit = InstrumentProperty(unit=AMPERE, range=(-1.05*AMPERE, 1.05*AMPERE), prefab=(float, ":SENS:CURR:PROT") )    
    
    voltage = InstrumentProperty(unit=VOLT, range=(-210*VOLT, 210*VOLT), prefab=(float, ":SOUR:VOLT:LEV") )    
    current = InstrumentProperty(unit=AMPERE, range=(-1.05*AMPERE, 1.05*AMPERE), prefab=(float, ":SOUR:CURR:LEV") )   
    
    
class Keithley2401(Keithley24XX):
    source_voltage_range = InstrumentProperty(unit=VOLT, range=(-40*VOLT, 40*VOLT), prefab=(float, ":SOURCE:VOLT:RANGE") )    
    source_current_range = InstrumentProperty(unit=AMPERE, range=(-1.05*AMPERE, 1.05*AMPERE), prefab=(float, ":SOURCE:CURR:RANGE") )    
        
    '''compliance limits'''
    voltage_limit = InstrumentProperty(unit=VOLT, range=(-21*VOLT, 21*VOLT), prefab=(float, ":SENS:VOLT:PROT") )    
    current_limit = InstrumentProperty(unit=AMPERE, range=(-1.05*AMPERE, 1.05*AMPERE), prefab=(float, ":SENS:CURR:PROT") )    

    voltage = InstrumentProperty(unit=VOLT, range=(-21*VOLT, 21*VOLT), prefab=(float, ":SOUR:VOLT:LEV") )    
    current = InstrumentProperty(unit=AMPERE, range=(-1*AMPERE, 1*AMPERE), prefab=(float, ":SOUR:CURR:LEV") )   
    

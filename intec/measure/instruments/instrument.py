from .instrument_base import InstrumentRegisteredAttribute
from .property import InstrumentProperty
from intec.units.si import MILLISECOND

class InstrumentMetaClass(type):
    def __new__(cls, name, bases, attr, link_dict = True):
        T = super(InstrumentMetaClass, cls).__new__(cls, name, bases, attr)
        assert len(bases) < 2
        for name, value in attr.items():
            if isinstance(value, InstrumentRegisteredAttribute):
                value.bind_to_class__(T, name)
        if bases:  # all clases derived from Instrument
            T.bases__ = bases[0].bases__ + [bases[0]]
        else: # Instrument
             T.bases__ = []
        T.attr__ = attr
        if link_dict:
            T.link_dict__ = {}
        return T

class Instrument(metaclass=InstrumentMetaClass):
    def __enter__(self):
        return self
    def __exit__(self, exception_type, exception_value, traceback):
        self.close()

    link_name_2_link_class = {}
    @classmethod
    def register_link_class(cls, link_class, link_name):
        Instrument.link_name_2_link_class[link_name] = link_class
        instrument_class = Instrument.link_name_2_instrument_class.get(link_name)
        if instrument_class:
            Instrument.link_dict__[link_class] = instrument_class

    link_name_2_instrument_class = {}
    @classmethod
    def register_instrument_class(cls, instrument_class, link_name):
        instrument_class.link_dict__ = Instrument.link_dict__
        Instrument.link_name_2_instrument_class[link_name] = instrument_class
        link_class = Instrument.link_name_2_link_class.get(link_name)
        if link_class:
            Instrument.link_dict__[link_class] = instrument_class

    @classmethod
    def new__(cls, link):
        cls2 = cls.link_dict__.get(link.__class__)
        if cls2:
            i = super(Instrument, cls2).__new__(cls2)
            i.link = link
            return i
        bases = list(cls.bases__)
        bases_to_build = []
        build = False
        while 1:
            cls2 = bases.pop(-1)
            cls3 = cls2.link_dict__.get(link.__class__)
            if cls3:
                if cls3 != cls2:
                    build = True
                break
            else:
                bases_to_build.append(cls2)
        bases_to_build.reverse()
        bases_to_build.append(cls)
        if build:
            for cls2 in bases_to_build:
                cls3 = InstrumentMetaClass.__new__(InstrumentMetaClass, cls2.__name__, (cls3,), cls2.attr__, link_dict = False)
                cls3.link_dict__ = cls2.link_dict__
                cls3.link_dict__[link.__class__] = cls3
        else:
            for cls2 in bases_to_build:
                cls3 = cls2
                cls3.link_dict__[link.__class__] = cls3
        i = super(Instrument, cls3).__new__(cls3)
        i.link = link
        return i

    def __init__(self, *args, **kwargs):
        self.__init_instrument__(*args, **kwargs)
    def __init_instrument__(self, *args, **kwargs):
        pass        

class VisaInstrument(Instrument):
    def __new__(cls, *args, **kwargs):
        link = kwargs.get('link')
        if not link:
            from ..link.visa import link_visa
            link = link_visa(kwargs['address'])
            i = super(VisaInstrument, cls).__new__(cls)
            i.link = link
            return i
        return cls.new__(link)
    def close(self):
        self.link.close()
    def write(self, *args, **kwargs):
        return self.link.write(*args, **kwargs)
    def read(self, *args, **kwargs):
        return self.link.read(*args, **kwargs)
    def query(self, *args, **kwargs):
        return self.link.query(*args, **kwargs)
    def query_int(self, *args, **kwargs):
        return int(self.link.query(*args, **kwargs))
    def query_float(self, *args, **kwargs):
        return float(self.link.query(*args, **kwargs))
    def query_bool(self, *args, **kwargs):
        return int(self.link.query(*args, **kwargs)) != 0
    def idn(self):
        return self.link.query("*IDN?").rstrip()
    timeout = InstrumentProperty(unit=MILLISECOND)
    def timeout_set(self, v):
        self.link.timeout = v
    def timeout_get(self):
        return self.link.timeout

from intec.units.si import VOLT, AMPERE
from intec.measure.libinstr.generator import *

class E36300_channel(Module):
    name = FixedProperty(unit=str)
    index = FixedProperty(unit=int)
    voltage = Property(unit=VOLT)
    voltage_min = FixedProperty(ref=voltage)
    voltage_max = FixedProperty(ref=voltage)
    voltage_def = FixedProperty(ref=voltage)
    current = Property(unit=AMPERE)

    C = CommandSet()
    C.command(target = voltage, scpi = "[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude] <voltage>|MINimum|MAXimum|DEFault (@{{index}})")

class E36300(Chassis, x=1):
    CH1 = E36300_channel(names=('CH1','P6V'))
    CH2 = E36300_channel(names=('CH2','P25V'))
    CH3 = E36300_channel(names=('CH3','N25V'))
    instrument_select = Property(choice=E36300_channel)
    C = CommandSet()
    C.command(target = instrument_select, scpi = "INSTrument[:SELect] P6V|P25V|N25V|CH1|CH2|CH3")
    C.command(target = E36300_channel.voltage, scpi = "[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude] <voltage>|MINimum|MAXimum|DEFault (@{{index}})")


class E36311A(E36300):
    pass
E36311A.CH1.voltage.range = ()
E36311A.CH2.voltage.range = ()
E36311A.CH3.voltage.range = ()

class E36312A(E36300):
    pass
E36312A.CH1.voltage.range = ()

class E36313A(E36300):
    pass
E36313A.CH1.voltage.range = ()

######################################################################################################################

class E36300_channel_interface(Interface, i=E36300_channel):
    pass
class E36300_interface(Interface, i=E36300):
    pass
class E36311A_interface(E36300_interface, i=E36311A):
    pass
class E36312A_interface(E36300_interface, i=E36312A):
    pass
class E36313A_interface(E36300_interface, i=E36313A):
    pass




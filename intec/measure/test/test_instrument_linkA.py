from intec.measure.instruments.instrument import Instrument

class linka__:
    def zoink(self, i):
        return i+1
Instrument.register_link_class(linka__, "linka")

def link_A(**kwargs):
    return linka__()


class linkb__:
    def zoink(self, i):
        return None
    def boink(self, i):
        return i-1
Instrument.register_link_class(linkb__, "linkb")

def link_B(**kwargs):
    return linkb__()



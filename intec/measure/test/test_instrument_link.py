import unittest
                
from intec.measure.instruments.instrument import Instrument
from intec.measure.instruments.property import InstrumentProperty
from intec.units.si import AMPERE, VOLT, MILLIAMPERE, MILLIVOLT


####################################################################################################

class test_instrument_link(unittest.TestCase):

    def test1(self):

        class LinkA_Instrument(Instrument):
            def __new__(cls, *args, **kwargs):
                link = kwargs.get('link')
                if not link:
                    from test_instrument_linkA import link_A
                    link = link_A(**kwargs)
                    i = super(LinkA_Instrument, cls).__new__(cls)
                    i.link = link
                    return i
                return cls.new__(link)
            def zoink(self, i):
                return self.link.zoink(i)
        Instrument.register_instrument_class(LinkA_Instrument, "linka")

        class LinkB_Instrument(Instrument):
            def __new__(cls, *args, **kwargs):
                link = kwargs.get('link')
                if not link:
                    from test_instrument_linkA import link_B
                    link = link_B(**kwargs)
                    i = super(LinkB_Instrument, cls).__new__(cls)
                    i.link = link
                    return i
                return cls.new__(link)
            def boink(self, i):
                return self.link.boink(i)
            def zoink(self, i):
                return self.link.zoink(i)
        Instrument.register_instrument_class(LinkB_Instrument, "linkb")

        A = LinkA_Instrument(addy = 1)
        self.assertEqual(A.zoink(2),3)
        from test_instrument_linkA import link_A, link_B
        link = link_A(addy=2)
        B = LinkA_Instrument(link=link)
        self.assertEqual(B.zoink(2),3)
        link = link_B(slot=3)
        C = LinkA_Instrument(link=link)
        self.assertEqual(C.zoink(2),None)
        self.assertEqual(C.boink(2),1)

        class AXXX(LinkA_Instrument):
            a = InstrumentProperty(unit = VOLT)
            b = InstrumentProperty(unit = AMPERE)

        class A001(AXXX):
            b = InstrumentProperty(unit = AMPERE)
        D = A001(link=link)
        self.assertEqual(D.zoink(2),None)
        self.assertEqual(D.boink(2),1)

        



        







    


if __name__ == '__main__':
    unittest.main()


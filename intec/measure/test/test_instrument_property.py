import unittest
                
from intec.measure.instruments.instrument import Instrument
from intec.measure.instruments.property import InstrumentProperty
from intec.units.si import AMPERE, VOLT, MILLIAMPERE, MILLIVOLT


class test_instrument_property(unittest.TestCase):

    def test_1(self):
        class P001(Instrument):
            max_current = InstrumentProperty()
            def max_current_get__(self):
                return self.mc - 1*AMPERE
            def max_current_set__(self, v):
                self.mc = v
        i = P001()
        i.max_current = 200*AMPERE
        self.assertEqual(i.max_current, 199*AMPERE)

        class P002(Instrument):
            max_current = InstrumentProperty(unit=AMPERE)
            def max_current_get__(self):
                return self.mc - 1
            def max_current_set__(self, v):
                self.mc = v
        i = P002()
        i.max_current = 200*AMPERE
        self.assertEqual(i.max_current, 199*AMPERE)
        with self.assertRaises(TypeError):
            i.max_current = 200*VOLT
        self.assertEqual(i.max_current, 199*AMPERE)

        class P003(Instrument):
            max_current = InstrumentProperty(unit=AMPERE, max_value = 1*AMPERE)
            def max_current_get__(self):
                return self.mc - 1
            def max_current_set__(self, v):
                self.mc = v
        i = P003()
        with self.assertRaises(TypeError):
            i.max_current = 200*VOLT
        with self.assertRaises(ValueError):
            i.max_current = 2*AMPERE
        with self.assertRaises(ValueError):
            i.max_current = 1001*MILLIAMPERE
        i.max_current = 998*MILLIAMPERE
        self.assertTrue(i.max_current.near(-2*MILLIAMPERE))
        
        class P004(Instrument):
            max_current = InstrumentProperty(allowed_values = [1,2,3,4,"MOO"])
            def max_current_get__(self):
                return self.mc - 1
            def max_current_set__(self, v):
                self.mc = v
        i = P004()
        with self.assertRaisesRegex(ValueError, "should be one of"):
            i.max_current = 0
        with self.assertRaisesRegex(ValueError, "should be one of"):
            i.max_current = "FOO"
        i.max_current = "MOO"
        i.max_current = 4
        self.assertEqual(i.max_current, 3)

        class P005(Instrument):
            max_current = InstrumentProperty(unit=VOLT, allowed_values = [1,2,3,4,"MOO"])
            def max_current_get__(self):
                return self.mc
            def max_current_set__(self, v):
                self.mc = v
        i = P005()
        with self.assertRaisesRegex(ValueError, "should be one of"):
            i.max_current = 0
        with self.assertRaisesRegex(ValueError, "should be one of"):
            i.max_current = "FOO"
        i.max_current = "MOO"
        i.max_current = 4 * VOLT
        self.assertEqual(i.max_current, 4 * VOLT)        
        i.max_current = "MOO"
        self.assertEqual(i.max_current, "MOO")        

        class P006(Instrument):
            voltage = InstrumentProperty(unit=VOLT, range=(0*VOLT, 20*VOLT))
            def voltage_get__(self):
                return self.mc
            def voltage_set__(self, v):
                self.mc = v
        i = P006()
        i.voltage = 10*VOLT
        with self.assertRaisesRegex(ValueError, "outside range supported"):        
            i.voltage = 21*VOLT
            self.assertEqual(i.voltage, 10000 * MILLIVOLT)
        with self.assertRaisesRegex(ValueError, "outside range supported"):        
            i.voltage = -1*VOLT
            self.assertEqual(i.voltage, 10000 * MILLIVOLT)
            
        class P007(Instrument):
            v1 = InstrumentProperty(unit=VOLT, max_value=(20*VOLT))
            def v1_get__(self):
                return self.mc
            def v1_set__(self, v):
                self.mc = v
        i = P007()
        i.v1 = 10*VOLT
        with self.assertRaisesRegex(ValueError, "above maximum supported by the device"):        
            i.v1 = 21*VOLT
            self.assertEqual(i.v1, 10000 * MILLIVOLT)
        i.v1 = -1*VOLT
        self.assertEqual(i.v1, -1000 * MILLIVOLT)

        class P008(Instrument):
            def __init_instrument__(self):
                self.D = {}
            def write(self, s):
                s = s.split(' ', 1)
                self.D[s[0]] = s[1]
            def ask(self, s):
                return self.D[s[:-1]]
            v1 = InstrumentProperty(unit=VOLT, range=(0*VOLT, 20*VOLT), prefab=(float, ':SOUR:VOLT:LEV'))
        i = P008()
        i.v1 = 10*VOLT
        self.assertEqual(i.v1, 10 * VOLT)
        with self.assertRaisesRegex(ValueError, "outside range supported"):        
            i.v1 = 21*VOLT
            self.assertEqual(i.v1, 10000 * MILLIVOLT)
        i.v1 = 0*VOLT
        self.assertEqual(i.v1, 0 * MILLIVOLT)


if __name__ == '__main__':
    unittest.main()


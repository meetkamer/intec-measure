import inspect
import os

def new_instrument(manufacturer, kind, type, subtype = None):
    util_folder = os.path.dirname( os.path.abspath(__file__) )
    instruments_folder = os.path.normpath( util_folder + "/../instruments")
    manufacturer_folder= instruments_folder + "/" + manufacturer
    if not os.path.exists(manufacturer_folder):
        os.mkdir(manufacturer_folder)
        open(manufacturer_folder + "/__init__.py", "wb")
    kind_file = manufacturer_folder + "/" + kind + ".py"
    F = open(kind_file, "a", encoding='utf-8')
    if subtype is None:
        subtype = type
    print("from .%s.%s import %s"%(type, type, subtype), file=F)
    type_folder = manufacturer_folder + "/" + type
    if not os.path.exists(type_folder):
        os.mkdir(type_folder)
        open(type_folder + "/__init__.py", "w")
    type_file = type_folder + "/" + type + ".py"
    if not os.path.exists(type_file):
        F = open(type_folder + "/" + type + ".py", "w")
    test_folder = type_folder + "/" + "test"
    if not os.path.exists(test_folder):
        os.mkdir(test_folder)
    test_file = test_folder + "/test_" + subtype + ".py"
    if not os.path.exists(test_file):
        F = open(test_file, "w")
        print("from intec.instruments.%s.%s import %s"%(manufacturer, kind, subtype), file=F)
        
        
        
if __name__ == '__main__':
    new_instrument("Agilent" , "signal_analyzer", "X_Series", "EXA_N9010A")
    new_instrument("Tektronix" , "oscilloscope", "TBS1000B", "TBS1052B")
    new_instrument("Tektronix" , "digital_serial_analyzer", "DSA8300")
    new_instrument("Anritsu" , "OSA", "MS9740A")
    new_instrument("Keopsys" , "optical_amplifier", "CEFA_C_HG_B201")
    new_instrument("Agilent" , "oscilloscope", "InfiniiVision6000", "DSO6104A")
    new_instrument("Tektronix" , "function_generator", "AFG3000", "AFG3102")
    #new_instrument("Keithley" , "voltage_current_source", "Keithley24XX", "Keithley2401")
    new_instrument("Keysight" , "power_supply", "E36300", "E36312A")
    input("ready")    
        


if (0)
{   
    $pythons = @(34,35,36,37,38)
    $pythons | % {
        & "c:\virtualenv\units\$_\scripts\pip.exe" install --upgrade "dist/intec_pymeasure-0.0.1-cp$_-cp$($_)m-win_amd64.whl"
        & "c:\virtualenv\units\$_-32\scripts\pip.exe" install --upgrade "dist/intec_pymeasure-0.0.1-cp$_-cp$($_)m-win32.whl"
    }
}

if (0)
{   
    $pythons = @(34,35,36,37,38)
    
    pushd intec\_test\units
    $pythons | % {
        & "c:\virtualenv\units\$_\scripts\python.exe" test_units.py
        & "c:\virtualenv\units\$_-32\scripts\python.exe" test_units.py
    }
    popd
}

Remove-Item .\dist -Force -Recurse -erroraction ignore
Remove-Item .\build -Force -Recurse -erroraction ignore


$pythons = @(34,35,36,37,38)
$pythons | % {
    & "c:\python$_-32\python.exe" setup.py bdist_wheel
    Remove-Item .\build -Force -Recurse
    & "c:\python$_\python.exe" setup.py bdist_wheel
    Remove-Item .\build -Force -Recurse
}

